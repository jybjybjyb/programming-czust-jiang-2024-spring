
# Stage 4.1
Write a C program to check if a word is palindrome or not. A palindrome is a string that reads 
the same backward as well as forward.
Some palindromes words are:
civic, radar, level, rotor, kayak, racecar, redder, madam, 1234321,….
```
Example Output 1:
Input a string: madam
Palindrome.
Example Output 2:
Input a string: programming
Not a palindrome.
```
# Stage 4.2
Write a program in C that calculates both the sum of all elements in each row and the sum of 
all elements in each column, of a square matrix.


![alt text](./fig-4-1.png)


# Stage 4.3
Write a program in C to concatenate two strings with a white space in the middle:
```
Example Output:
Enter the 1rd String: Smith
Enter the 2nd String: Martin
The concatenate String is: Smith Martin
```

# Stage 4.4
Write a program in C that compare two names alphabetically (without using strcmp) and find 
out which name is alphabetically first. The names can have uppercase or lowercase letters.
```
Example Output 1:
Enter the 1rd Name: Thomas
Enter the 2nd Name: Thompson
The 1rd String precedes the 2nd alphabetically.
Example Output 2:
Enter the 1rd Name: Jones
Enter the 2nd Name: Johnson
The 2nd Name precedes the 1st alphabetically.
Example Output 3:
Enter the 1rd Name: Smith
Enter the 2nd Name: Smith
The Names are equals.
```
# Stage 6.1
Clearing the Least Significant Bit (Bit 0) in C:
Write a C program that clears (sets to zero) the least significant bit (bit 0) of an integer number entered by the user, without changing the other bits.

Setting the Most Significant Bit (Bit 31) in C:
Create a C program that sets (turns on) the most significant bit (bit 31) of an integer input provided by the user, while keeping the other bits unchanged.

Toggling the Third Bit (Bit 2) in C:
Develop a C program that toggles (flips) the third bit (bit 2) of an integer value entered by the user, leaving the remaining bits unaffected.

# Stage 6.2 
Demonstrate that your previous solution works by printing the equivalent binary of 
the user’s entered number, before and after the Clear operation.

Hint: use the iota function.

# Stage 6.3
Setting Bit 4 in C:
Write a C program that sets (turns on) bit 4 of an integer number entered by the user, without changing the other bits. After setting bit 4, display the equivalent binary, decimal, and hexadecimal representations of the modified number.

Clearing Bit 7 in C:
Develop a C program that clears (sets to zero) bit 7 of an integer input provided by the user, while keeping the other bits unchanged. Show the binary, decimal, and hexadecimal values before and after the operation.

Toggling Bit 2 in C:
Create a C program that toggles (flips) bit 2 of an integer value entered by the user, leaving the remaining bits unaffected. Print the binary, decimal, and hexadecimal representations before and after the toggle.


# Stage 6.4 
Write a C program that accepts an integer from the user (within the range 0 to 31, both inclusive) and calculates the number of set bits (i.e., 1) in its binary representation using bitwise operations.

# Stage 6.5 
Write a C program that determines whether an entered integer is even or odd using bitwise operations.

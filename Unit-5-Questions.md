
# Stage 5.1
Write a C program that prints all prime numbers between number 2 to a number entered by 
the user. Use a user-defined function checkPrimeNumber to checks whether a number is 
prime or not.
A prime number is a positive integer which is divisible only by 1 and by itself. 
For example: 2, 3, 5, 7, 11, 13,..
```
Example Output:
Enter the end of the prime number list: 30
2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
```

# Stage 5.2
Write a C program that asks the user a number and displays a pyramid of rows, as shown 
below in case of 5 rows:
```
Input number of rows: 5
    *
   ***
  *****
 *******
*********
```
In the program define a function printChs that has among its input parameters an integer 
number n and a character. The function displays a row of n characters. 


# Stage 5.3
Write a C program that gets a character from keyboard and checks if it is a Number or a 
Letter and, in case it is a Letter, check if it is a Consonant or a Vowel. It ends when a 
character other than these is entered.
```
Output:
Enter a Character from Keyboard: 1 is a Number.
Enter a Character from Keyboard: 2 is a Number.
Enter a Character from Keyboard: 3 is a Number.
Enter a Character from Keyboard: 4 is a Number.
Enter a Character from Keyboard: 5 is a Number.
Enter a Character from Keyboard: a is a Vowel.
Enter a Character from Keyboard: b is a Consonant.
Enter a Character from Keyboard: c is a Consonant.
Enter a Character from Keyboard: d is a Consonant.
Enter a Character from Keyboard: e is a Vowel.
Enter a Character from Keyboard: ! is neither a Number nor a Letter!...
```


# Stage 5.4
Write a C Program that reads any positive integer and displays the word equivalent of each 
number’s digit.
```
E.g. 
Input: 192837465
Output: One Nine Two Eight Three Seven Four Six Five
```

# Stage 5.5
Write a C program to allow users to enter the size and elements of a one dimensional array. 
It then prints minimum, maximum, average values and sorted array elements in ascending 
order.
In the main function use external functions for each request.
```
Example Output:
Enter the number of elements in the array: 5
Number 1 = 17
Number 2 = 23
Number 3 = 10
Number 4 = 5
Number 5 = 45
Minimum = 5.00
Maximum = 45.00
Average = 20.00
Array in ascending order: 5.00, 10.00, 17.00, 23.00, 45.00,
```

# Stage 5.6
Write a C program to allow users to enter a number of Names and store them in a two 
dimensional Array. Print the names in alphabetic order without using the function strcmp(). 
The names all consists of lowercase letters.


# Stage 5.7
Write a C program to allow users to enter a number of Names and store them in a two 
dimensional Array. Print the names in alphabetic order without using the function strcmp(). 
The Surname can have uppercase or lowercase letters.

```
Example Output:

Enter the number of the Names: 5
Name 1: peter
Name 2: oliver
Name 3: william
Name 4: emily
Name 5: oscar
The names in alphabetical order are:
emily
oliver
oscar
peter
william
Example Output:
Enter the number of the Names: 5
Name 1: Peter
Name 2: oliver
Name 3: William
Name 4: emily
Name 5: Oscar
The names in alphabetical order are:
emily
oliver
Oscar
Peter
William

```
# Stage 3.1
Write a C program that reads two positive integer numbers and checks what is the largest integer that 
can exactly divide both numbers (with zero as remainder).
```
Example Output 1:
Enter first positive integer number: 4
Enter second positive integer number: 22
The largest integer that can exactly divide both numbers is: 2
Example Output 2:
Enter first positive integer number: 9
Enter second positive integer number: 3
The largest integer that can exactly divide both numbers is: 3
Example Output 3:
Enter first positive integer number: 18
Enter second positive integer number: 5
The largest integer that can exactly divide both numbers is: 1
```
# Stage 3.2
Write a C program that reads a number and checks whether a given number is positive or negative, 
even or odd. It ends when zero is entered.
```
Example Output:
Input an integer: 2
2 is positive and even.
Input an integer: -2
-2 is negative and even.
Input an integer: 5
5 is positive and odd.
Input an integer: -5
-5 is negative and odd.
Input an integer: 0
0 is even.
```


# Stage 3.3
Write a program in C to display the multiplication table of a given integer. 

```
Example Output :
Input the number: 5
5 X 1 = 5
5 X 2 = 10
5 X 3 = 15
5 X 4 = 20
5 X 5 = 25
5 X 6 = 30
5 X 7 = 35
5 X 8 = 40
5 X 9 = 45
5 X 10 = 50
```

# Stage 3.4
A prime number is a number that is only divisible by 1 and itself.
Write a C program that reads a positive number and checks if it is a prime number. It ends when the 
number one is entered.
We can check whether a number x is a prime number by checking if it is divisible by any of the 
numbers between 2 to number / 2. 
```
Example Output:
Enter a integer Number: 2
2 is a Prime Number
Enter a integer Number: 3
3 is a Prime Number
Enter a integer Number: 4
4 is NOT a Prime Number
Enter a integer Number: 5
5 is a Prime Number
Enter a integer Number: 6
6 is NOT a Prime Number
Enter a integer Number: 7
7 is a Prime Number
Enter a integer Number: 8
8 is NOT a Prime Number
Enter a integer Number: 1
Thanks, Bye
```


# Stage 3.5
Write a C program that reads a number and prints out a multiplication table of numbers 1,2,…,n.
The output must be shown in a two-dimensional grid of n rows and n columns, as shown below for 
the case n = 5:
```
 1 2  3   4   5
 2 4  6   8   10
 3 6  9   12  15
 4 8  12  16  20
 5 10 15  20  25
 ```
You can use the special "\t" character within printf() function to get a clear output.


# Stage 3.6
Write a C program that asks the user a number and displays a triangle of stars, as shown below in 
case of n = 5:
Input number of rows : 5
```
*
**
***
****
*****
```


# Stage 3.7
Write a C program that asks the user a number and displays a pyramid of rows, as shown below in 
case of 5 rows:
Input number of rows : 5
 ```
     *
    ***
   *****
  *******
 *********
```
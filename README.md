# Programming-CZUST-jiang-2024s

This repo is post for the 4ENT1179 Programming, including lesson labs, phase tests, and other exercises. 

## Course Information 

CODE: 4ENT1179

NAME: Programming

TEACHER: Dr.Jiang

EMAIL: jiangyb@czust.edu.cn

## Lesson Tasks

Authorship: individual

Weighting (%):

Important: In each individual lesson you will be given several tasks (or questions). For all the following tasks, don't forget to make notes of your observations and findings, as you will need them later for your lab report. Also note (code) any attempts that did not work.

## Contribution
Authorship: Individual

Weighting (%):

In this course many questions will be posted on this website (in the "issue" label on the page). YOU can ask or answer any question related to that lesson. The asking or answering actions will be considered by the TEACHERS into a score (called Presentation Score in Chinese).  

## Timetable

| Room | Lecture & Lab | Lecture & Lab Topics | Time |
|---|---|---|---|
| Yaogua (C303) | Lecture: Introduction to C Language and Variables | Lab: Getting familiar with C Language and Variables | 13:30 - 17:00 |
| Yaogua (C303) | Lecture: Variables, Conditions and Loops | Lab: Variables, Conditions and Loops | 13:30 - 17:00 |
| Kaiyang(C303) | Lecture: Conditions and Loops | Lab: Conditions and Loops | 13:30 - 17:00 |
| Kaiyang (C303) | Lecture: Loops and Arrays | Lab: Loops and Arrays | 13:30 - 17:00 |
| Kaiyang(C303) | Lecture: Arrays | Lab: Arrays | 13:30 - 17:00 |
| Kaiyang (C303) | 2:00 PM Phase Test 1 (in class) | | 13:30 - 17:00 |
| Kaiyang(C303) | Lecture: Functions and Bitwise Operations | Lab: Functions and Bitwise Operations | 13:30 - 17:00 |
| Kaiyang(C303) | Lecture: Functions and Bitwise Operations | Lab: Functions and Bitwise Operations | 13:30 - 17:00 |
| Kaiyang (C303) | Lecture: Bitwise Operations | Lab: Bitwise Operations | 13:30 - 17:00 |
| Kaiyang(C303) | 2:00 PM Phase Test 2 (in class) | | 13:30 - 17:00 |
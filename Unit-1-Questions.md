# Computer Programming for Electronics Engineers – Lab 1 – Variables - Questions Only
## Stage 1.1
Write a program in C that converts temperatures from Celsius to Fahrenheit.

Hint:
Fahrenheit = ( (9 / 5) * Celsius) + 32 

```
Example Output:
Temperature in Celsius: 10
10.00 degrees Celsius is 50.00 degrees Fahrenheit.
```
## Stage 1.2
Write a C program that gets in input a character and prints the equivalent 
number of the ASCII table. Then, it gets in input an integer number, from 33 to 
254, and prints the equivalent character of the ASCII table.
```
Example Output:
Enter a character: A
The character 'A' = 65
Enter a integer number (from 33 to 254): 66
The number 66 = 'B' 
```

## Stage 1.3
Write a C program to estimate a motorbike’s average consumption from the 
given total distance (in km) and consumed fuel (in liters).

Hint:
Average consumption in liters = km traveled / consumed fuel
```
Example Output:
Input total distance in km: 100
Input total fuel spent in litres: 7
Average consumption is: 14.29 km/lt
```

## Stage 1.4
Write a C program that accepts two resistor values (in ohms) and calculates
the resulting resistance if connected in series and in parallel.

Hints:
Resistance in Series RTot = R1 + R2;
Resistance in Parallel RTot = (R1 * R2) / (R1 + R2).

```
Example Output:
Please enter the first resistor value (in ohms): 5.6
Please enter the second resistor value (in ohms): 10
Total resistance in Series is 15.60 ohms
Total resistance in Parallel is 3.59 ohms
```

## Stage 1.5
Write a C program that accepts fathoms and converts them to inches, hands, 
feet, yards and meters.

Hints:
One fathom is equal to:
 72 inches (1 inches is 1/72 of a fathom);
 18 hands (1 hands is 1/16 of a fathom);
 6 feet (1 foot is 1/6 of a fathom);
 2 yards (1 yard is 1/2 of a fathom);
 1.8288 meters (1 meter is 0.54680665 of a fathom).
```
Example Output:
Enter the depth in fathoms: 10
Its depth at sea: 10.0000 fathoms is:
720.0000 inches
180.0000 hands
60.0000 feet
20.0000 yards
18.2880 metres
```

## Stage 1.6
Write a C program that accepts the Width and the Height of a Rectangle and 
compute Perimeter and Area.

Hints:
 Perimeter = (width + height) *2;
 Area = width * height.

```
Example Output:
Insert width: 10
Insert height: 7
Perimeter of the rectangle = 34 inches
Area of the rectangle = 70 square inches
```

## Stage 1.7
Write a C program that accepts a Radius of a circle and computes Perimeter 
and Area.

Hints:
 Perimeter = 2 π R;
 Area = π R^2;
 π = 3.14.

```
Example Output:
Insert Radius of a Circle (inches): 5
Perimeter of the Circle = 31.40 inches
Area of the Circle = 78.50 square inches
```

## Stage 1.8
Write a C program that accepts a number of days and converts the specified 
days into years, weeks and days.

Hints:
 You can use the % operator to find the remainder of a division;
 years = days / 365;
 weeks = (days % 365) / 7;
 days = days - ((years * 365) + (weeks * 7)).

```
Example Output:
Insert Days: 1000
Years: 2
Weeks: 38
Days: 4
```
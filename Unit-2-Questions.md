## Stage 2.1 
Write a C program that accepts two integers and checks about whether: they have the 
same value; the first one is greater than the second one; the first one is smaller than the 
second one. 
```
Example Output 1: 
Input the values for Number 1: 9 
Input the values for Number 2: 4 
Number 1 is greater than Number 2 
Example Output 2: 
Input the values for Number 1: 3 
Input the values for Number 2: 7 
Number 2 is greater than Number 1 
Example Output 3: 
Input the values for Number 1: 5 
Input the values for Number 2: 5 
Number 1 and Number 2 are equal  
```

## Stage 2.2 
Write a C program that checks about whether a given number is positive or negative, even 
or odd. 
```
Example Output 1: 
Input an integer: -3 -3 is negative and odd. 
Example Output 2: 
Input an integer: -2 -2 is negative and even. 
Example Output 3: 
Input an integer: 0 
0 is even. 
Example Output 4: 
Input an integer: 2 
2 is positive and even. 
Example Output 5: 
Input an integer: 3 
3 is positive and odd. 
```

## Stage 2.3 
Write a C program that reads two integers and checks if they are multiplies of each other. 
```
Example Output 1: 
Input the first number: 3 
Input the second number: 9 
Multiples! 
Example Output 2: 
Input the first number: 17 
Input the second number: 6 
Not Multiples! 
```

## Stage 2.4 
Write a C program that reads three numbers and prints them in ascending order. 
```
Example Output 1: 
Input the first number: 4 
Input the second number: 8 
Input the third number: 2 
The numbers in ascending order are: 2, 4, 8  
```

## Stage 2.5 
Write a C program that reads temperatures in Celsius and displays the suitable message 
according to the below table: - - - - - - 
Temp < 0 then Freezing weather; 
Temp 0-9 then Very Cold weather; 
Temp 10-19 then Cold weather; 
Temp 20-29 then Normal in Temp; 
Temp 30-39 then It’s Hot; 
Temp >=40 then It’s Very Hot. 

```
Example Output 1: 
Input temperature: -5 
Freezing weather. 
Example Output 2: 
Input temperature: 15 
Cold weather. 
Example Output 2: 
Input temperature: 25 
Normal temperature.  
```

## Stage 2.6 
Write a C program that reads an alphabetic character and checks if it is Vowel or Consonant. 
```
Example Output 1: 
Enter an alphabetic character: a 
The alphabetic character 'a' is a vowel. 
Example Output 2: 
Enter an alphabetic character: b 
The alphabetic character 'b' is a consonant. 
Example Output 3: 
Enter an alphabetic character: + 
'+' is not an alphabetic character. 
```

## Stage 2.7 
Write a C program that reads three floating values, checks whether it is possible to have a 
triangle out of the three values, and if it is possible it calculates the triangle’s Perimeter. 
The program should also check about whether a triangle is Equilateral, Isosceles or Scalene. 

Equilateral triangle: An equilateral triangle is a triangle in which all three sides are 
equal; 

Isosceles triangle: An isosceles triangle is a triangle that has two sides of equal length; 

Scalene triangle: A scalene triangle is a triangle that has three unequal sides. 

![alt text](./fig-2-1.png)

```
Examples: 
Example Output 1: 
Input the first number: 7.2 
Input the second number: 7.2 
Input the third number: 7.2 
This is an equilateral triangle and the Perimeter is = 21.60 
Example Output 2: 
Input the first number: 5.5 
Input the second number: 5.5 
Input the third number: 4.3 
This is an isosceles triangle and the Perimeter is = 15.30 
Example Output 3: 
Input the first number: 7.2 
Input the second number: 5.5 
Input the third number: 4.3 
This is a scalene triangle and the Perimeter is = 17.00 
```

## Stage 2.8 
Write a Program in C to create a simple calculator: 
```
Output: 
Enter an operator (+, -, *, /): + 
Enter first operand: 32.5 
Enter second operand: 12.4 
32.5 + 12.4 = 44.9
```